import psycopg2
import time
from collections import OrderedDict
import fiona
from fiona.crs import from_epsg

UNCLASSIFIED = 0
NOISE = -1
current_cluster = 1
queries = 0

schema = {
  'geometry': 'Point',
  'properties': OrderedDict([
    ('id', 'int'),
    ('time', 'str'),
    ('animal', 'int'),
    ('cluster', 'int'),
  ])
}

animal_crs = from_epsg(3857)
output_driver = 'ESRI Shapefile'

class Animal:
  def __init__(self, *args, **kwargs):
    self.id = int(kwargs['id'])
    self.time = kwargs['time']
    self.animal = int(kwargs['animal'])
    self.coordinates = kwargs['coordinates']
    self.cluster = UNCLASSIFIED

  @property
  def as_schema(self):
    return {
      'geometry': {
        'type': 'Point',
        'coordinates': self.coordinates
      },
      'properties': OrderedDict([
        ('id', self.id),
        ('time', self.time),
        ('animal', self.animal),
        ('cluster', self.cluster),
      ]),
    }



def queryRegion(id, eps):
  global queries
  c = connection.cursor()
  c.execute('select id from animale as a where st_dwithin(a.geom, (select geom from animale where id={0} limit 1), {1}) and id != {0}'.format(id, eps))
  queries += 1

  return set([row[0] for row in c ])


def expandCluster(pointSet, pointId, clId, eps, minPts ):
  seeds = queryRegion(pointId, eps)

  if len(seeds) < minPts:
    pointSet[pointId].cluster = NOISE
    return False
  else:
    pointSet[pointId].cluster = clId

    while len(seeds) > 0:
      current_seed_id = seeds.pop()
      results = queryRegion(current_seed_id, eps)

      if len(results) >= minPts:
        for p in results:
          if pointSet[p].cluster in (UNCLASSIFIED, NOISE):
            if pointSet[p].cluster == UNCLASSIFIED:
              seeds.update([p])

            pointSet[p].cluster = clId

      seeds.discard(current_seed_id)

    return True


if __name__ == '__main__':

  # TODO: put in .env
  connection = psycopg2.connect("dbname=geo user=nicokant")
  cursor = connection.cursor()

  # TODO: put as args
  eps = 300
  minPts = 20

  cursor.execute('select id, animal, time, x, y from animale')

  points = { row[0]: Animal(id=row[0], animal=row[1], time=row[2], coordinates=(row[3], row[4])) for row in cursor }

  start = time.time()
  for id in points.keys():
    animal = points[id]
    if animal.cluster == UNCLASSIFIED:
      if expandCluster(points, id, current_cluster, eps, minPts):
        current_cluster += 1

  elapsed = time.time() - start

  print('total clusters: {}'.format(current_cluster - 1))
  print('total noise: {}'.format(len(filter(lambda animal: animal.cluster == NOISE, points.values()))))
  print('total points: {}'.format(len(points)))
  print('executed {} queries'.format(queries))
  print('took {} seconds'.format(elapsed))

  with fiona.open('results.shp', 'w', driver=output_driver, schema=schema, crs=animal_crs) as shp:
    for animal in points.values():
      shp.write(animal.as_schema)
