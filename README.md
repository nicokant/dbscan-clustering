# DBSCAN PY
A python implementation example of the DBSCAN clustering algorithm for the Geospatial Information Management course at University of Milan

## Setup
Install dependencies using Pipenv
```
pipenv install
```

## Run
Run the script using Pipenv virtualenv
```
pipenv run python3 main.py
```

## Result
It will produce a shapefile (.shp), import it in a GIS software like QGIS or ArcMap

![](result.png)
